﻿using UnityEngine;
using UnityEditor;
using System.IO;
using System.Linq;
public static class CustomMenu{

	[MenuItem("Tools/Create New GameObject")]
	public static void CreateNewGameObject(){
		GameObject obj = new GameObject();
		obj.AddComponent<SpriteRenderer>();
		obj.AddComponent<CommonSetting>();
	}

	static string contents = "";

	[MenuItem("Tools/Get Hierarchy List")]
	public static void GetHierarchyList(){
		GetHierarchyList_("Assets/Resources");
		contents = contents.Replace('\\','/');
		File.WriteAllText("Assets/Resources/DataFile/HierarchyList.txt", contents);
		AssetDatabase.Refresh();
	}

	static void GetHierarchyList_( string dir ){
		string[] paths = Directory.GetDirectories(dir);
		foreach ( string path in paths ){
			#if UNITY_EDITOR_WIN
				contents += path.Replace("Assets/Resources\\","")+"\n";
			#else
				contents += path.Replace("Assets/Resources/","")+"\n";
			#endif

			// フォルダ内にプレハブがあれば、それも生成する.
			string[] prefabList = System.IO.Directory.GetFiles (path, "*.prefab" );
			foreach ( string prefabPath in prefabList ){
				#if UNITY_EDITOR_WIN
					contents += prefabPath.Replace("Assets/Resources\\","")+"\n";
				#else
					contents += prefabPath.Replace("Assets/Resources/","")+"\n";
				#endif
			}
			GetHierarchyList_(path);
		}
	}
/*
	[MenuItem("Tools/Apply Prefab State")] 
	static void ApplyPrefabState () { 
		Selection.gameObjects.ApplyPrefabState (false); 
	} 
	[MenuItem("Tools/Apply Prefab State", true)] 
	static bool ValidateApplyPrefabState () { 
		return Selection.gameObjects.All (prefabInstance => prefabInstance.IsPrefabInstance ()); 
	}
*/
/*	プロにしたら使ってみたい.
	[MenuItem("Build/Android & iOS")]
	public static void Build(){
		BuildPlayer("Android.apk", BuildTarget.Android, BuildOptions.None);
		BuildPlayer("iOS", BuildTarget.iPhone, BuildOptions.None);
	}

	static void BuildPlayer(string location, BuildTarget target, BuildOptions options){
		Directory.CreateDirectory("Build");
		BuildPipeline.BuildPlayer(GetLevels (), "Build/" + location, target, options);
	}

	static string[] GetLevels(){
	return EditorBuildSettings.scenes.
		Where(scene => scene.enabled).Select(scene => scene.path).ToArray();
	}
*/
}
