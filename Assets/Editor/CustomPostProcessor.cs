﻿using UnityEngine;
using System;
using System.IO;
using System.Xml;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.Callbacks;
using UnityEditor.XCodeEditor;
using MiniJSON;


public static class CustomPostProcessor
{

	[PostProcessBuild (100)]
	public static void OnPostProcessBuild (BuildTarget target, string path)
	{
		if (target == BuildTarget.iPhone) {
			PostProcessBuild_iOS (path);
		}
	}
	

	private static void PostProcessBuild_iOS (string path)
	{
		CreateModFile (path);
		ProcessXCodeProject (path);
	}
	
	private static void CreateModFile (string path)
	{
		Dictionary<string, object> mod = new Dictionary<string, object> ();
		
		List<string> patches = new List<string> ();
		List<string> librarysearchpaths = new List<string> ();
		List<string> folders = new List<string> ();
		List<string> excludes = new List<string> ();
		

		List<string> libs = new List<string> ();
		

		List<string> frameworksearchpaths = new List<string> ();

		List<string> frameworks = new List<string> ();
		frameworks.Add ("QuartzCore.framework");
		frameworks.Add ("CoreImage.framework:weak");
		frameworks.Add("AdSupport.framework");
		frameworks.Add("CFNetwork.framework");
		frameworks.Add("CoreData.framework");
		frameworks.Add("CoreGraphics.framework");
		frameworks.Add("CoreLocation.framework");
		frameworks.Add("CoreMotion.framework");
		frameworks.Add("CoreTelephony.framework");
		frameworks.Add("EventKitUI.framework");
		frameworks.Add("EventKit.framework");
		frameworks.Add("Foundation.framework");
		frameworks.Add ("libsqlite3.0.dylib");
		frameworks.Add("libxml2.dylib");
		frameworks.Add("libz.dylib");
		frameworks.Add("MapKit.framework");
		frameworks.Add("MediaPlayer.framework");
		frameworks.Add("MessageUI.framework");
		frameworks.Add("MobileCoreServices.framework");
		frameworks.Add("PassKit.framework");
		frameworks.Add("QuartzCore.framework");
		frameworks.Add("Security.framework");
		frameworks.Add("Social.framework");
		frameworks.Add("StoreKit.framework");
		frameworks.Add("SystemConfiguration.framework");
		frameworks.Add("Twitter.framework");
		frameworks.Add("UIKit.framework");
		frameworks.Add("GameKit.framework");
		frameworks.Add("ImageIO.framework");


		List<string> files = new List<string> ();
		

		List<string> headerpaths = new List<string> ();
		

		Dictionary<string,List<string>> buildSettings = new Dictionary<string,List<string>> ();
		List<string> otherLinkerFlags = new List<string> ();
		otherLinkerFlags.Add ("-ObjC");
		buildSettings.Add ("OTHER_LDFLAGS", otherLinkerFlags);
		
		mod.Add ("group", "");
		mod.Add ("patches", patches);
		mod.Add ("libs", libs);
		mod.Add ("librarysearchpaths", librarysearchpaths);
		mod.Add ("frameworksearchpaths", frameworksearchpaths);
		mod.Add ("frameworks", frameworks);
		mod.Add ("headerpaths", headerpaths);
		mod.Add ("files", files);
		mod.Add ("folders", folders);
		mod.Add ("excludes", excludes);
		mod.Add ("buildSettings", buildSettings);
		

		string jsonMod = Json.Serialize (mod);
		
		string file = System.IO.Path.Combine (path, "CustomXCode.projmods");
		
		if (File.Exists (file)) {
			File.Delete (file);
		}
		
		using (StreamWriter streamWriter = File.CreateText (file)) {
			streamWriter.Write (jsonMod);
		}
	}
	

	private static void ProcessXCodeProject (string path)
	{
		XCProject project = new XCProject (path);
		
		string[] files = System.IO.Directory.GetFiles (path, "*.projmods", System.IO.SearchOption.AllDirectories);
		
		foreach (string file in files) {
			project.ApplyMod ( System.IO.Path.Combine (Application.dataPath, file));
		}
		
		project.Save ();
	}
}