﻿using UnityEngine;
using UnityEditor;
 using UnityEditor.Callbacks;
public sealed class Importer : AssetPostprocessor
{
 
	//returnの値が小さいほど優先される.
	public override int GetPostprocessOrder()
	{
		return 0;
	}
 
	//==================================================================
	//Audio設定.
	//==================================================================
 
	//Audioファイルのインポート設定 Audioファイルがインポートされる直前に呼び出される
	void OnPreprocessAudio()
	{
		//インポート時のAudioファイルを設定するクラス
		AudioImporter audioImporter = assetImporter as AudioImporter;
 
		//各設定
		audioImporter.forceToMono = false;			//モノラルにするか
		audioImporter.format = AudioImporterFormat.Compressed;	//フォーマット設定 Native or Compressed
		audioImporter.threeD = false;				//3Dサウンドにするか
		audioImporter.compressionBitrate = 96000; 	//ビットレートの設定[kbps]
		audioImporter.hardware = true;				//iOS上でオーディオを圧縮するか
		audioImporter.loopable = false;				//AndroidまたはiOS上でループを保持するか
 
 
		//読み込み時設定
		//DecompressOnLoad ロード中にサウンドを解凍．圧縮されたサウンドを解凍する時１０倍以上のメモリを使用．大きいファイルには不向き．
		//CompressedInMemory メモリ上に圧縮したサウンドを保持，再生中に解凍．オーバヘッドが小さい．サイズが大きいファイル向き
		//StreamFromDisc 直接オーディデータを流す。こ非常に長い曲向き。
		audioImporter.loadType = AudioImporterLoadType.DecompressOnLoad;
 
	}
 
	//==================================================================
	//Texture設定
	//==================================================================
 
	//Textureファイルのインポート設定 Textureファイルがインポートされる直前に呼び出される
	void OnPreprocessTexture()
	{
		//インポート時のTextureファイルを設定するクラス
		TextureImporter importer = assetImporter as TextureImporter;
		TextureImporterSettings settings = new TextureImporterSettings();
		importer.ReadTextureSettings(settings);
		//settings.spriteAlignment = (int)SpriteAlignment.TopLeft;
		importer.SetTextureSettings(settings);
		//各設定
        importer.textureType = TextureImporterType.Sprite;
        importer.spriteImportMode = SpriteImportMode.Single;
        importer.alphaIsTransparency = false;

        int maxTextureSize;
        TextureImporterFormat textureImporterFormat;
        if (!importer.GetPlatformTextureSettings("iPhone", out maxTextureSize, out textureImporterFormat)) {
            importer.SetPlatformTextureSettings("iPhone", 2048, TextureImporterFormat.PVRTC_RGBA4, (int)TextureCompressionQuality.Fast);
        }else if (!importer.GetPlatformTextureSettings("Android", out maxTextureSize, out textureImporterFormat)) {
            importer.SetPlatformTextureSettings("Android", 2048, TextureImporterFormat.ATC_RGBA8, (int)TextureCompressionQuality.Fast);
        }else{
	        importer.compressionQuality = (int)TextureCompressionQuality.Fast;
	        importer.maxTextureSize = 2048;
	        importer.textureFormat = TextureImporterFormat.RGBA32;        	
        }
	}
}