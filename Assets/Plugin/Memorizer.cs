﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Collections.Generic;
public static class Memorizer {
	public static Dictionary<string,string> list = new Dictionary<string,string>();


	public static bool HasKey(string key){
		return list.ContainsKey(key);
	}

	public static void SetString(string key, string value){
		if ( list.ContainsKey(key) ){
			list[key] = value;
		}else{
			list.Add(key,value);
		}
	}

	public static string GetString(string key){
		return list[key];
	}

	public static string SAVE_FILEPATH = Application.persistentDataPath + "/save_data";
	public static void Save(){
	    if (!Directory.Exists(Application.persistentDataPath)){
	        Directory.CreateDirectory(Application.persistentDataPath);
	    }
	    string data = "";
	    foreach (KeyValuePair<string,string>pair in list){
	    	data += "" + pair.Key + "," + pair.Value + "\n";
	    	Debug.Log("Saved ["+pair.Key+"] = "+pair.Value+"");
	    }
	    File.WriteAllText(SAVE_FILEPATH, data, System.Text.Encoding.UTF8);
	}

	public  static void Load(){
		if (!File.Exists(SAVE_FILEPATH)){
			Save();
		}
	    string data = File.ReadAllText(SAVE_FILEPATH, System.Text.Encoding.UTF8);
	    string[] tmps = data.Split("\n"[0]);
	    foreach (string tmp in tmps ){
	    	string[] t = tmp.Split(","[0]);
	    	if ( t.Length == 2 ){
	    		list.Add(t[0],t[1]);
	    		Debug.Log("Loaded ["+t[0]+"] = "+t[1]+"");
	    	}
	    }    
	}
}
