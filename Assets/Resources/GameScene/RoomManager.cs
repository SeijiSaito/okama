﻿using UnityEngine;
using System.Collections;

public class RoomManager : MonoBehaviour {

	DataManager DM;
	public GameObject box;

	public float baseX = 66f;
	public float baseY = 150f;
	public float dx = 116f;
	public float dy = 116f;
	// Use this for initialization
	void Start () {
		DM = DataManager.Instance;
		StartCoroutine(Init());
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	IEnumerator Init(){
		while ( !DM.isLoadedAll ){
			yield return new WaitForSeconds(0.1f);
		}
		for (int i = 0; i < DM.tate; i++) {
			for(int j = 0; j < DM.yoko; j++){
				GameObject obj = Instantiate (box) as GameObject;
				obj.transform.parent = transform;
				obj.name = "room"+((i*DM.yoko)+j);
				obj.GetComponent<CommonSetting>().x = baseX + j * dx;
				obj.GetComponent<CommonSetting>().y = baseY + i * dy;
				obj.SetActive(true);
			}
		}
	}
}
