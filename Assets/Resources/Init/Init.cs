﻿using UnityEngine;
using System.Collections;
using System.IO;
public class Init : MonoBehaviour {
	// 最初に表示したいシーン;
	string startScene = "TitleScene";
	
	void Start(){

		// VSyncをOFFにする;
		QualitySettings.vSyncCount = 0;

		// ターゲットフレームレートを30に設定;
		Application.targetFrameRate = 30; 
		SceneCreator.Instance.CreateDirectories(startScene);
		Destroy(gameObject);
	}
}
