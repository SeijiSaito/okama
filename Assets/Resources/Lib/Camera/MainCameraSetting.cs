﻿using UnityEngine;
using System.Collections;

/**
 * @brief Camera Setting
 * @details [long description]
 */
public class MainCameraSetting : SMonoBehaviour<MainCameraSetting> {
	public float limitTopY = 0f;
	public float limitBottomY = 0f;

	Vector3 pos;
	void Start () {
		Camera cam = GetComponent<Camera> ();
		cam.orthographicSize = Global.BaseScreenHeight / Global.PixelToUnits / 2;
		float baseAspect = (float)Global.BaseScreenHeight / (float)Global.BaseScreenWidth;
		float nowAspect = (float)Screen.height / (float)Screen.width;
		float changeAspect;
		if (baseAspect > nowAspect) {
			changeAspect = nowAspect / baseAspect;
			cam.rect = new Rect ((1.0f - changeAspect) * 0.5f, 0.0f, changeAspect, 1.0f);
		} else {
			changeAspect = baseAspect / nowAspect;
			cam.rect = new Rect (0.0f, (1.0f - changeAspect) * 0.5f, 1.0f, changeAspect);
		}
		cam.depth = 0;
		cam.backgroundColor = new Color (0,0,0);
		transform.position = new Vector3( 0, 0, Global.TopZ );
		pos = transform.position;
	}
	float accel = 0f;
	float vector = 0f;
	float ddy = 0f;
	void Update(){
		if ( ddy < 0f ){
			float tmp = ddy/10f;
			pos.y += tmp;
			ddy -= tmp;
		}else if ( ddy > 0f ){
			float tmp = ddy/10f;
			pos.y += tmp;
			ddy -= tmp;
		}
		// 画面制限とアニメーション.
		if ( pos.y > limitTopY/Global.PixelToUnits ){
			vector = -0.01f;
			if ( pos.y < limitTopY/Global.PixelToUnits+0.2f ){
				vector = 0f;
				accel = 0f;
				pos.y = limitTopY/Global.PixelToUnits;
			}
		}else if( pos.y < limitBottomY/Global.PixelToUnits ){
			vector = 0.01f;
			if ( pos.y > limitBottomY/Global.PixelToUnits-0.2f ){
				vector = 0f;
				accel = 0f;
				pos.y = limitBottomY/Global.PixelToUnits;
			}
		}
		accel += vector;
		if ( accel > 1f ) accel = 1f;
		pos.y += accel;
		transform.position = pos;
	}
	public void OnSlide( float dy ){
		pos = transform.position;
		ddy -= dy/Global.PixelToUnits;
	}
	public void ChangeLimitBottom(float y){
		limitBottomY = y;
		pos.y = 0f;
		vector = 0f;
		accel = 0f;
		ddy = 0f;
	}
}
