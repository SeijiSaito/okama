﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DataManager : SMonoBehaviour<DataManager> {

	public float touchableTime;
	public int enemyRate;
	public int dummyRate;
	public int heartRate;
	public float interval;
	public int tate;
	public int yoko;
	public int life;

	void Awake () {
		Memorizer.Load();
		LoadHighScore ();
		StartCoroutine(CheckLoadStatus());
	}
	void OnApplicationQuit(){
		SaveSaveData();
		Memorizer.Save();
		Debug.Log("お疲れさまでした");
	}

	void SaveSaveData(){
		for (int i = 1; i < 5; i++) {
			string key = "Top" + i;
			Memorizer.SetString(key,HighScore[key]);
		}
	}

	public bool isLoadedAll = false;
	// 読み込み進捗.
	const int targetLoadNum = 1;
	int loadProgress = 0;

	IEnumerator CheckLoadStatus(){
		isLoadedAll = false;
		while (loadProgress!=targetLoadNum){
			yield return new WaitForSeconds(0.1f);
			Debug.Log("progress:"+(decimal)loadProgress/(decimal)targetLoadNum*100+"%");
		}
		isLoadedAll = true;
	}
		
	// ハイスコア;
	Dictionary<string,string> highScore;
	public Dictionary<string,string> HighScore{
		get{
			if (isLoadedAll) return highScore;
			else Debug.Log("isLoadedAll = false");
			return null;	
		}
	}


	// ハイスコアの読み込み;
	void LoadHighScore(){
		highScore = new Dictionary<string,string> ();
		for (int i = 1; i < 5; i++) {
			string key = "Top" + i;
			if (Memorizer.HasKey (key)) {
				highScore.Add (key, Memorizer.GetString (key));
			} else {
				highScore.Add (key, "0");
			}
		}
		loadProgress++;
	}
}
