﻿using UnityEngine;
using System.Collections;

/**
 * @brief InputManager
 * @details 入力の監視人
 */
public class InputManager : SMonoBehaviour<InputManager> {
	void Update(){
		Close ();
		TouchObject ();
	}

	public bool isLockEscape;
	public void Close(){
		// iPhoneはバックキーが使えないのではじいておく;
		if ( Global.isIPhone ) return; 

		if ( Input.GetKey(KeyCode.Escape) && !isLockEscape ){
			//SceneCreator.Instance.CreateDirectories("ProdScene",1);
			isLockEscape = true;
		}else{
			isLockEscape = false;
		}

	}
	public bool TouchBegan( int finger = 0 ){
		if ( Global.isEditor ){
			return Input.GetMouseButtonDown(0);
		}
		if ( Global.isAndroid || Global.isIPhone ){
			if ( Input.touchCount > finger ){
				return Input.GetTouch(finger).phase == TouchPhase.Began;
			}			
		}
		return false;
	}
	public bool TouchEnded( int finger = 0 ){
		if ( Global.isEditor ){
			return Input.GetMouseButtonUp(0);
		}
		if ( Global.isAndroid || Global.isIPhone ){
			if ( Input.touchCount > finger ){
				return Input.GetTouch(finger).phase == TouchPhase.Ended;
			}			
		}
		return false;
	}
	public Vector3 TouchPosition( int finger = 0 ){
		if ( Global.isEditor ){
			return Input.mousePosition;
		}
		if ( Global.isAndroid || Global.isIPhone ){
			if ( Input.touchCount > finger ){
				return Input.GetTouch(finger).position;
			}			
		}
		return Vector3.zero;
	}
	public bool TouchObject(){
		// タッチトリガー;
		if ( !TouchBegan() ){
			return false;
		}
		Ray ray = new Ray();
		RaycastHit2D hit = new RaycastHit2D();
		ray = Camera.main.ScreenPointToRay(TouchPosition());
		hit = Physics2D.Raycast(ray.origin,ray.direction);
		if ( hit.collider != null ){
			GameObject target = hit.collider.gameObject;
			if ( target != null ){
				target.SendMessage("OnTouch",SendMessageOptions.DontRequireReceiver);
				return true;
			}
		}
		return false;
	}
	
}
