﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Collections.Generic;

/**
 * @brief ディレクトリ構成から自動生成;
 * @details 今回のプロジェクトの大事な部分;
 * 
 */
public class SceneCreator : SMonoBehaviour<SceneCreator> {
	public GameObject nowScene = null; 
	Dictionary<string, GameObject> cache = new Dictionary<string, GameObject>();
	public TextAsset dirList;


	/**
	 * @brief ディレクトリ自動生成;
	 * @details [long description]
	 * 
	 * @param scene 遷移するシーン;
	 * @param mode 現在シーンの対処 0 = 削除しない 1 = 非アクティブにする 2 = Destroyする;
	 */
	public void CreateDirectories ( string scene, int mode = 0 ) {
		GameObject root;

		if ( nowScene != null ){
			// 現在のシーンと遷移しようとしているシーンが同じなら特になにもしない;
			if ( nowScene.name == scene ) return;
		}

		if ( dirList == null ){
			// リストを読み込んでいない場合は読み込む;
			dirList = Resources.Load("DataFile/HierarchyList") as TextAsset;
		}
		Debug.Log(dirList);
		// シーン共通オブジェクトがない場合は作成する;
		if ( GameObject.Find("CommonScene") == null ){

			// ルートディレクトリの作成;
			root = new GameObject();
			root.name = "CommonScene";

			// ここから再帰的に自動生成していく;
			CreateDirectories_("CommonScene",root,dirList.text.Split("\n"[0]));
		}

		// あとで対処するので移しておく;
		GameObject oldScene = nowScene;

		// シーンがキャッシュに残っていない場合は生成する;
		if ( !cache.ContainsKey(scene) ){

			// ルートディレクトリの作成;
			root = new GameObject();
			root.name = scene;

			// ここから再帰的に自動生成していく;
			CreateDirectories_(scene,root,dirList.text.Split("\n"[0]));

			nowScene = root;
		}else{
			// キャッシュに残っていたのでアクティブにする;
			cache[scene].SetActive(true);

			nowScene = cache[scene];
		}

		// シーンが生成されたので古いシーンの対処をする;
		if ( mode == 0 ){
			// 何もしない;
		}else if ( mode == 2 ){
			// 削除する;
			Destroy(oldScene);
		}else if ( mode == 1 ){
			// 非アクティブ状態だと、次回復帰時に探せないのでキャッシュに残しておく;
			if ( !cache.ContainsKey(oldScene.name) ){
				cache.Add(oldScene.name, oldScene);
			}

			// 非アクティブにする;
			oldScene.SetActive(false);
		}
	}

	void CreateDirectories_( string dir, GameObject parent, string[] fileList, string relativePath = "" ){
		if ( relativePath == "" ){
			relativePath = dir;
		}
		List<string> res = new List<string>();
		List<string> list = new List<string>();
		foreach ( string path in fileList ){
			if ( path.IndexOf(dir+"/") == -1 ) continue;
			string tmp = path.Replace(dir+"/","");
			list.Add(tmp);
			if ( tmp.IndexOf("/") != -1 ) continue;
			res.Add(tmp);
			
			
		}
		foreach ( string path in res ){
			if ( path.IndexOf("/") == -1 && path.IndexOf(".prefab") >= 0 ){
				// フォルダ内にプレハブがあれば、それも生成する;
				// パスをResources.Load用に加工する;
				string prefabName = relativePath + "/" + path.Replace(".prefab","");
				Debug.Log(prefabName);
				GameObject prefab = Resources.Load(prefabName) as GameObject;
				GameObject objPrefab = Instantiate( prefab ) as GameObject;

				// プレハブ設定;
				objPrefab.transform.parent = parent.transform;
				objPrefab.name = System.IO.Path.GetFileNameWithoutExtension(prefabName);
			}else{
				// フォルダ生成;
				Debug.Log(path);
				GameObject obj = new GameObject();
				obj.transform.parent = parent.transform;
				obj.name = path;	
				CreateDirectories_(path,obj,list.ToArray(),dir+"/"+path);
			}
		}
	}
}
