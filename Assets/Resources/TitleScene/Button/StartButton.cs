﻿using UnityEngine;
using System.Collections;

public class StartButton : MonoBehaviour {
	DataManager DM;
	bool isUpdate;
	// 次のシーン遷移;
	string nextScene = "GameScene";
	void Start(){
		DM = DataManager.Instance;
		StartCoroutine (Init ());

	}
	IEnumerator Init(){
		while (!DM.isLoadedAll) {
			yield return new WaitForSeconds (0.1f);
		}
		isUpdate = true;
	}
	void OnTouch(){
		if (!isUpdate) return;
		if ( GameObject.Find(nextScene) ) return;
		SceneCreator.Instance.CreateDirectories(nextScene,1);
	}
}
