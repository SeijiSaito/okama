﻿using UnityEngine;
using System.Collections;

/**
 * @brief Position Set
 * @details [long description]
 */
public class CommonSetting : MonoBehaviour {
	public float x;
	public float y;
	public float z;
	public bool isCameraFollow;
	public bool isAutoAddCollision;
	public bool isAutoAddOnlyTrigger = false;
	Vector2 v;

	void Start(){
		if ( isAutoAddCollision ){
			if ( gameObject.GetComponent<BoxCollider2D>() != null ){
				Destroy(gameObject.GetComponent<BoxCollider2D>());
			}
			gameObject.AddComponent<BoxCollider2D>();
		}
		if (isAutoAddOnlyTrigger) {
			gameObject.GetComponent<BoxCollider2D> ().isTrigger = true;
		}
	}

	void Update () {
		v = new Vector2( Global.BaseTopLeftX + x/Global.PixelToUnits, Global.BaseTopLeftY - y/Global.PixelToUnits );
		if ( isCameraFollow ){
			v.y += Camera.main.transform.position.y;
		}
		transform.position = new Vector3(v.x,v.y,z);
	}
}
