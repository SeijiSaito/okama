﻿// テストモード切替（本番に影響させないようにする）.
#define TEST_MODE

// 広告を消す.
#define HIDDEN_AD

using UnityEngine;
using System.Collections;
using System.IO;
/**
 * @brief Global Const 環境設定.
 * @details [long description]
 * 
 */
public static class Global{

	// 画面の解像度.
	public static float BaseScreenWidth = 480f;
	public static float BaseScreenHeight = 800f;
	public static float PixelToUnits = 100f;
	public static float BaseTopLeftX = -(( BaseScreenWidth * 0.5f ) / PixelToUnits );
	public static float BaseTopLeftY = ( BaseScreenHeight * 0.5f ) / PixelToUnits;	
	public static float TopZ = -100f;
	public static float BottomZ = 100f;

	// プラットフォーム.
#if UNITY_EDITOR
	public static readonly bool  isEditor = true;
	public static readonly string STREAMING_PATH = "file://" + Application.streamingAssetsPath;
	public static readonly string RESOURCES_PATH = Application.dataPath + "/Resources/";
#else
	public static readonly bool  isEditor = false;
#endif

#if UNITY_ANDROID && !UNITY_EDITOR
	public static readonly bool  isAndroid = true;
	public static readonly string STREAMING_PATH = "" + Application.streamingAssetsPath;
	public static readonly string RESOURCES_PATH = Application.dataPath + "/Resources/";
#else
	public static readonly bool  isAndroid = false;
#endif

#if UNITY_IPHONE && !UNITY_EDITOR
	public static readonly bool  isIPhone = true;
	public static readonly string STREAMING_PATH = "file://" + Application.streamingAssetsPath;
	public static readonly string RESOURCES_PATH = Application.dataPath + "/Resources/";
#else
	public static readonly bool  isIPhone = false;
#endif

	// 使用サーバ.
#if TEST_MODE
	public static readonly string SERVER_PATH = "http://fl-game.com/spapp_gp/develop";
#else
	public static readonly string SERVER_PATH = "http://fl-game.com/spapp_gp/develop";
#endif
#if TEST_MODE
	public static bool isTestMode = true;
#else
	public static bool isTestMode = false;
#endif
	public static Sprite Tex2DToSprite(Texture2D tex){
		Rect rect = new Rect(0,0,tex.width,tex.height);
		return Sprite.Create(tex,rect,new Vector2(0f,1f),Global.PixelToUnits);
	}


}


#if TEST_MODE
public static class Debug
{
	static int cnt=0;
	public static string[] log = new string[20];
    static public void Break(){
        if( IsEnable() )    UnityEngine.Debug.Break();
    }

    static public void Log( object message ){
        if( IsEnable() ){
        	log[(++cnt)%20] = message as string ;
            UnityEngine.Debug.Log( message );
        }
    }
    static public void Log( object message, Object context ){
        if( IsEnable() ) {
            UnityEngine.Debug.Log( message, context );
        }
    }
    static public void LogWarning( object message ){
    	UnityEngine.Debug.LogWarning( message );
    }
    static public void LogError( object message ){
    	UnityEngine.Debug.LogError( message );
    }

    static bool IsEnable(){ return true; }
}
#else
public static class Debug
{
	static int cnt=0;
	public static string[] log = new string[20];
    static public void Break(){
    }

    static public void Log( object message ){
    }
    static public void Log( object message, Object context ){
    }

    static public void LogWarning( object message ){
    	UnityEngine.Debug.LogWarning( message );
    }
    static public void LogError( object message ){
    	UnityEngine.Debug.LogError( message );
    }

    static bool IsEnable(){ return false; }
}
#endif