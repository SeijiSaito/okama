﻿using UnityEngine;
using System.Collections;

public class PhysicsSetting : MonoBehaviour {
	public float x;
	public float y;
	public float z;
	public bool isCameraFollow;
	Vector2 v;

	void Start(){

	}

	void Update () {
		v = new Vector2( Global.BaseTopLeftX + x/Global.PixelToUnits, Global.BaseTopLeftY - y/Global.PixelToUnits );
		if ( isCameraFollow ){
			v.y += Camera.main.transform.position.y;
		}
		transform.position = new Vector3(v.x,v.y,z);
	}
}
