﻿using UnityEngine;

/**
 * @brief [brief description]
 * @details naichilab.blogspot.jp/2013/11/unitymanager.html
 * 
 * @param e [description]
 * @return [description]
 */
public class SMonoBehaviour<T> : MonoBehaviour where T : MonoBehaviour {
		private static T instance;
		public static T Instance{
				get{
						if ( instance == null ){
								instance = (T)FindObjectOfType(typeof(T));
								Debug.Log(typeof(T)+" is nothing");
						}
						return instance;
				}
		}
}
